//
//  RecipeListModel.swift
//  Recipe book for ios
//
//  Created by Archangel on 25.04.2019.
//  Copyright © 2019 Archangel. All rights reserved.
//

import Foundation
import UIKit

struct RecipeList
{
    var uuid: String
    var name: String
    var images: Array<String>
    var image: UIImage?
    var lastUpdated: Int
    var description: String
    var instructions: String
    var difficulty: Int
    var combinedTextForSearch: String
    
    init?(json: [String: Any])
    {
        guard
            let uuid = json["uuid"] as? String,
            let name = json["name"] as? String,
            let images = json["images"] as? Array<String>,
            let lastUpdated = json["lastUpdated"] as? Int,
            let description = json["description"] as? String,
            let instructions = json["instructions"] as? String,
            let difficulty = json["difficulty"] as? Int
        else
            {return nil}
        
        self.uuid = uuid
        self.name = name
        self.images = images
        self.lastUpdated = lastUpdated
        self.description = description
        self.instructions = instructions
        self.difficulty = difficulty
        self.combinedTextForSearch = name + description + instructions
    }
    
    static func getArray(from jsonArray: Any) -> [RecipeList]?
    {
        guard let jsonArray = jsonArray as? Array<[String: Any]> else { return nil }
        var list: [RecipeList] = []
        
        for jsonObject in jsonArray {
            if let listVal = RecipeList(json: jsonObject) {
                list.append(listVal)
            }
        }
        return list
    }
}
