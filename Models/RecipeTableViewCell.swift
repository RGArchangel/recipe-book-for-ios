//
//  RecipeCellModel.swift
//  Recipe book for ios
//
//  Created by Archangel on 29/04/2019.
//  Copyright © 2019 Archangel. All rights reserved.
//

import Foundation
import UIKit

class RecipeTableViewCell: UITableViewCell
{
    @IBOutlet weak var cellView: UIView!
    @IBOutlet weak var recipeImage: UIImageView!
    @IBOutlet weak var nameOfRecipe: UILabel!
    @IBOutlet weak var descriptionOfRecipe: UILabel!
}
