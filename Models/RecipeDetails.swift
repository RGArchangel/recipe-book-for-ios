//
//  RecipeDetailsModel.swift
//  Recipe book for ios
//
//  Created by Archangel on 25.04.2019.
//  Copyright © 2019 Archangel. All rights reserved.
//

import Foundation

struct RecipeDetails
{
    var uuid: String
    var name: String
    var images: Array<String>
    var lastUpdated: Int
    var description: String
    var instructions: String
    var difficulty: Int
    var similar: Array<RecipeBrief>
    
    init?(json: [String: Any])
    {
        guard
            let uuid = json["uuid"] as? String,
            let name = json["name"] as? String,
            let images = json["images"] as? Array<String>,
            let lastUpdated = json["lastUpdated"] as? Int,
            let description = json["description"] as? String,
            let instructions = json["instructions"] as? String,
            let difficulty = json["difficulty"] as? Int,
            let jsonSimilar = json["similar"] as? [[String: String]]
        else
            {return nil}
        
        var similar: Array<RecipeBrief> = []
        for json in jsonSimilar
        {
            guard let brief = RecipeBrief(json: json) else {return nil}
            similar.append(brief)
        }
        
        let instructionsCorrect = instructions.replacingOccurrences(of: "<br>", with:"\n")
        
        self.uuid = uuid
        self.name = name
        self.images = images
        self.lastUpdated = lastUpdated
        self.description = description
        self.instructions = instructionsCorrect
        self.difficulty = difficulty
        self.similar = similar
    }
    
    static func getRecipe(from jsonValue: Any) -> RecipeDetails?
    {
        guard let jsonValue = jsonValue as? [String: Any] else {return nil}
        guard let recipe = RecipeDetails(json: jsonValue) else {return nil}
        
        return recipe
    }
}
