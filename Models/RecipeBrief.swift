//
//  RecipeBriefModel.swift
//  Recipe book for ios
//
//  Created by Archangel on 25.04.2019.
//  Copyright © 2019 Archangel. All rights reserved.
//

import Foundation

struct RecipeBrief
{
    var uuid: String
    var name: String
    
    init? (json: [String: Any])
    {
        guard
            let name = json["name"] as? String,
            let uuid = json["uuid"] as? String
        else
            {return nil}
        
        self.name = name
        self.uuid = uuid
    }
}
