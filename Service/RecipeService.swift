//
//  RecipeService.swift
//  Recipe book for ios
//
//  Created by Archangel on 08.04.2019.
//  Copyright © 2019 Archangel. All rights reserved.
//

import Foundation
import Alamofire

class RecipeService
{
    private enum Constants
    {
        static let baseURL = "https://test.kode-t.ru"
        static let error = NSError(domain: "RecipeService", code: 0, userInfo: nil)
    }
    
    private enum EndPoints
    {
        static let recipes = "/recipes"
        static let uuID = "/recipes/"
    }
    
    func obtainListOfRecipes(checkResponse: @escaping (_ failure: Bool) -> Void)
    {
        let url = URL(string: Constants.baseURL + EndPoints.recipes)!
        request(url,
                method: .get,
                parameters: nil,
                encoding: URLEncoding.default,
                headers: nil).validate().responseJSON { responseJSON in
                    switch responseJSON.result{
                    case .success(let data):
                        guard let json = data as? [String: Any] else { return }
                        guard let jsonArray = json["recipes"] else { return }
                        guard let list = RecipeList.getArray(from: jsonArray) else { return }
                        
                        self.saveList(getList: list)
                        checkResponse(false)
                        
                    case .failure:
                        checkResponse(true)
                    }
        }
    }
    
    func obtainChoosenRecipe(uuid: String, checkResponse: @escaping (_ failure: Bool) -> Void)
    {
        let url = URL(string: Constants.baseURL + EndPoints.uuID + uuid)!
        request(url,
                method: .get,
                parameters: nil,
                encoding: URLEncoding.default,
                headers: nil).validate().responseJSON { responseJSON in
                    switch responseJSON.result{
                    case .success(let data):
                        guard let json = data as? [String: Any] else {return}
                        guard let jsonValue = json["recipe"] as? [String: Any] else {return}
                        guard let recipe = RecipeDetails.getRecipe(from: jsonValue) else {return}
                        
                        self.saveRecipe(getRecipe: recipe)
                        checkResponse(false)
                        
                    case .failure:
                        checkResponse(true)
                    }
        }
    }
    
    //Work with list of recipes
    var list: Array<RecipeList>!
    func saveList(getList: Array<RecipeList>)
    {
        list = getList
    }
    
    func sendSortedList(sortedList: Array<RecipeList>)
    {
        list = sortedList
    }
    
    //Work with choosen recipe
    var recipe: RecipeDetails!
    func saveRecipe(getRecipe: RecipeDetails)
    {
        recipe = getRecipe
    }
}
