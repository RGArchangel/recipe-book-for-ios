//
//  AppDelegate.swift
//  Recipe book for ios
//
//  Created by Archangel on 08.04.2019.
//  Copyright © 2019 Archangel. All rights reserved.
//

import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        return true
    }

}

