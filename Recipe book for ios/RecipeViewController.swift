//
//  RecipeViewController.swift
//  Recipe book for ios
//
//  Created by Archangel on 12/05/2019.
//  Copyright © 2019 Archangel. All rights reserved.
//

import UIKit
import Auk
import QuartzCore

var choosenRecipeUuid: String?

class RecipeViewController: UIViewController, UIScrollViewDelegate {
    
    // MARK: - IBOutlets
    
    @IBOutlet weak var recipeScrollView: UIScrollView!
    @IBOutlet weak var imageDifficulty: UIImageView!
    @IBOutlet weak var difficultyLabel: UILabel!
    @IBOutlet weak var recipeDescription: UILabel!
    @IBOutlet weak var recipeInstructions: UILabel!
    @IBOutlet weak var similarButton: UIButton!
    @IBOutlet weak var nameOfTheRecipe: UILabel!
    @IBOutlet weak var imagesScrollView: UIScrollView!
    @IBOutlet weak var pageIndicator: UILabel!
    @IBOutlet weak var hudImage: UIImageView!
    @IBOutlet weak var recipeView: UIView!
    
    // MARK: - Public Classes
    
    let recipeService = RecipeService()
    
    // MARK: - Private Properties
    
    var currentImageIndex: Int = 0
    
    // MARK: - Working With Interface
    
    private func clearInformation()
    {
        hideElements()
        imagesScrollView.auk.removeAll()
    }
    
    private func hideElements()
    {
        hudImage.loadGif(name: "loadingGif")
        recipeView.isHidden = true
    }
    
    private func showElements()
    {
        hudImage.image = nil
        recipeView.isHidden = false
        
        if recipeService.recipe.similar.count == 0
        {similarButton.isHidden = true}
    }
    
    private func updatePageIndicator()
    {
        currentImageIndex = imagesScrollView.auk.currentPageIndex!
        pageIndicator.text = "\(currentImageIndex + 1)/\(recipeService.recipe.images.count)"
    }
    
    private func updateImages()
    {
        imagesScrollView.auk.removeAll()
        for imageURL in recipeService.recipe.images
        {
            imagesScrollView.auk.show(url: imageURL)
        }
    }
    
    private func setStartPreferences()
    {
        imagesScrollView.delegate = self
        imagesScrollView.auk.settings.pageControl.visible = false
        clearInformation()
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView)
    {
        updatePageIndicator()
    }
    
    @objc func dismissAlertController()
    {
        self.dismiss(animated: true, completion: nil)
    }
    
    // MARK: - Loading Functions
    
    private func loadRecipe()
    {
        hudImage.image = nil
        nameOfTheRecipe.text = recipeService.recipe.name
        updateImages()
        
        pageIndicator.text = "1/\(recipeService.recipe.images.count)"
        switch recipeService.recipe.difficulty {
        case 1:
            imageDifficulty.image = #imageLiteral(resourceName: "difficulty-1")
        case 2:
            imageDifficulty.image = #imageLiteral(resourceName: "difficulty-2")
        case 3:
            imageDifficulty.image = #imageLiteral(resourceName: "difficulty-3")
        case 4:
            imageDifficulty.image = #imageLiteral(resourceName: "difficulty-4")
        case 5:
            imageDifficulty.image = #imageLiteral(resourceName: "difficulty-5")
        default:
            return
        }
        
        recipeDescription.text = "Description: \n\(recipeService.recipe.description) \n"
        recipeInstructions.text = "Instructions: \n\(recipeService.recipe.instructions) \n\n"
        
        if (recipeService.recipe.similar.count != 0)
        {similarButton.isHidden = false}
        
        showElements()
    }
    
    private func changeRecipe(uuid: String)
    {
        choosenRecipeUuid = uuid
        let newRecipeViewController = UIStoryboard.init(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "RecipeViewController")
        self.navigationController?.pushViewController(newRecipeViewController, animated: true)
    }
    
    private func checkResponse(failure: Bool)
    {
        if failure
        {
            let alertController = UIAlertController(title: "Error", message: "Failed to load data", preferredStyle: .alert)
            let retryAction = UIAlertAction(title: "Retry", style: .default) { (action) -> Void in
                self.recipeService.obtainChoosenRecipe(uuid: choosenRecipeUuid!, checkResponse: self.checkResponse)
            }
            
            alertController.addAction(retryAction)
            self.present(alertController, animated: true, completion: nil)
        }
        else
        {loadRecipe()}
    }
    
    // MARK: - Lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setStartPreferences()
        recipeService.obtainChoosenRecipe(uuid: choosenRecipeUuid!, checkResponse: checkResponse)
    }
    
    // MARK: - IBActions
    
    @IBAction func tapTheImage(_ sender: UITapGestureRecognizer) {
        guard let imageIndex = imagesScrollView.auk.currentPageIndex else {return}
        let stringURL = recipeService.recipe.images[imageIndex]
        choosenImageUrl = URL(string: stringURL)
        performSegue(withIdentifier: "goToRecipeImage", sender: self)
    }
    
    @IBAction func similarButtonTouched(_ sender: UIButton) {
        let alertController = UIAlertController(title: "Similar recipes", message: "", preferredStyle: .actionSheet)
        
        for brief in recipeService.recipe.similar
        {
            let recipeBriefGate = UIAlertAction(title: brief.name, style: .default) { (action) -> Void in
                self.changeRecipe(uuid: brief.uuid)
            }
            alertController.addAction(recipeBriefGate)
        }
        
        self.present(alertController, animated: true) {
            let tapGesture = UITapGestureRecognizer(target: self, action: #selector(self.dismissAlertController))
            alertController.view.superview?.subviews[0].addGestureRecognizer(tapGesture)
        }
    }
    
}
