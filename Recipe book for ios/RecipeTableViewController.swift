//
//  ViewController.swift
//  Recipe book for ios
//
//  Created by Archangel on 08.04.2019.
//  Copyright © 2019 Archangel. All rights reserved.
//

import UIKit
import Foundation
import Kingfisher
import SwiftGifOrigin

class RecipeTableViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, UISearchBarDelegate
{
    
    // MARK: - IBOutlets
    
    @IBOutlet weak var recipesTable: UITableView!
    @IBOutlet weak var recipesSearch: UISearchBar!
    @IBOutlet weak var hudImage: UIImageView!
    @IBOutlet weak var sortButton: UIButton!
    @IBOutlet weak var cancelButton: UIBarButtonItem!
    
    // MARK: - Public Classes
    
    let recipeService = RecipeService()
    
    // MARK: - Private Properties
    
    private let cellReuseIdentifier = "recipeList"
    private var filteredList: Array<RecipeList> = []
    
    // MARK: - Working With Interface
    
    private func hideElements()
    {
        recipesTable.isHidden = true
        recipesSearch.isHidden = true
    }
    
    private func showElements()
    {
        recipesTable.isHidden = false
        recipesSearch.isHidden = false
    }
    
    private func disableButtons()
    {
        sortButton.isEnabled = false
        cancelButton.isEnabled = false
    }
    
    private func enableButtons()
    {
        sortButton.isEnabled = true
        cancelButton.isEnabled = true
    }
    
    private func setStartPreferences()
    {
        hudImage.loadGif(name: "loadingGif")
        hideElements()
        disableButtons()
    }
    
    private func enableInterface()
    {
        enableButtons()
        showElements()
        hudImage.image = nil
    }
    
    @objc func dismissAlertController()
    {
        self.dismiss(animated: true, completion: nil)
    }
    
    // MARK: - TableView Properties
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if recipeService.list == nil
        {return 0}
        else
        {return filteredList.count}
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell: RecipeTableViewCell = self.recipesTable.dequeueReusableCell(withIdentifier: cellReuseIdentifier) as! RecipeTableViewCell
        
        cell.nameOfRecipe.text = filteredList[indexPath.row].name
        cell.descriptionOfRecipe.text = filteredList[indexPath.row].description
        
        let url = URL(string: filteredList[indexPath.row].images.first!)
        cell.recipeImage.kf.setImage(with: url)
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.view.endEditing(true)
        let uuid = filteredList[indexPath.row].uuid
        choosenRecipeUuid = uuid
        performSegue(withIdentifier: "goToRecipe", sender: self)
    }
    
    // MARK: - Loading Functions
    
    private func reloadTable()
    {
        filteredList = recipeService.list
        recipesTable.reloadData()
        enableInterface()
    }
    
    private func scrollTableToTop()
    {
        if (filteredList.count == 0)
        {return}
        else
        {
            let indexPath = IndexPath(row: 0, section: 0)
            recipesTable.scrollToRow(at: indexPath, at: .top, animated: true)
        }
    }
    
    private func filterTable()
    {
        recipesTable.reloadData()
        scrollTableToTop()
    }
    
    private func checkResponse(failure: Bool)
    {
        if failure
        {
            let alertController = UIAlertController(title: "Error", message: "Failed to load data", preferredStyle: .alert)
            let retryAction = UIAlertAction(title: "Retry", style: .default) { (action) -> Void in
                self.recipeService.obtainListOfRecipes(checkResponse: self.checkResponse)
            }
            
            alertController.addAction(retryAction)
            self.present(alertController, animated: true, completion: nil)
        }
        else
        {reloadTable()}
    }
    
    // MARK: - Lifecycle
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        setStartPreferences()
        if (filteredList.count == 0)
        {recipeService.obtainListOfRecipes(checkResponse: checkResponse)}
    }
    
    // MARK: - IBActions
    
    @IBAction func sortListOfRecipes(sender: UIButton) {
        self.view.endEditing(true)
        
        let alertController = UIAlertController(title: "Sort by", message: "", preferredStyle: .actionSheet)
        
        let nameSortAction = UIAlertAction(title: "Name", style: .default) { (action) -> Void in
            var list = self.filteredList
            list.sort{$0.name < $1.name}
            self.filteredList = list
            self.filterTable()
        }
        
        let dateSortAction = UIAlertAction(title: "Date", style: .default) { (action) -> Void in
            var list = self.filteredList
            list.sort{$0.lastUpdated > $1.lastUpdated}
            self.filteredList = list
            self.filterTable()
        }
        
        alertController.addAction(nameSortAction)
        alertController.addAction(dateSortAction)
        
        self.present(alertController, animated: true) {
            let tapGesture = UITapGestureRecognizer(target: self, action: #selector(self.dismissAlertController))
            alertController.view.superview?.subviews[0].addGestureRecognizer(tapGesture)
        }
    }
    
    @IBAction func cancelSearchFilter(_ sender: UIBarButtonItem) {
        self.view.endEditing(true)
        recipesSearch.text = nil
        filteredList = recipeService.list
        reloadTable()
        scrollTableToTop()
    }
    
    // MARK: - Search Fucntion
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        self.view.endEditing(true)
        
        guard let searchText = recipesSearch.text else {return}
        if searchText == ""
        {
            filteredList = recipeService.list
            reloadTable()
            return
        }
        
        let filtered = self.recipeService.list.filter() {
            ($0.combinedTextForSearch.lowercased() as NSString).contains(searchText.lowercased())
        }
        
        filteredList = filtered
        recipesTable.reloadData()
        scrollTableToTop()
    }
        
}

