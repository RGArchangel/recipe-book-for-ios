//
//  RecipeImageController.swift
//  Recipe book for ios
//
//  Created by Archangel on 26/05/2019.
//  Copyright © 2019 Archangel. All rights reserved.
//

import Foundation
import UIKit
import Kingfisher
import Toast_Swift

var choosenImageUrl: URL?

class RecipeImageController: UIViewController, UIScrollViewDelegate {
    
    // MARK: - IBOutlets
    
    @IBOutlet weak var recipeImageField: UIScrollView!
    @IBOutlet weak var recipeImage: UIImageView!
    
    // MARK: - Preferences Functions
    
    private func setZoomScales()
    {
        self.recipeImageField.minimumZoomScale = 1.0
        self.recipeImageField.maximumZoomScale = 8.0
    }
    
    func viewForZooming(in scrollView: UIScrollView) -> UIView? {
        return recipeImage
    }
    
    private func setStartPreferences()
    {
        recipeImage.kf.setImage(with: choosenImageUrl)
        setZoomScales()
    }
    
    // MARK: - Lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setStartPreferences()
    }
    
    // MARK: - IBActions
    
    @IBAction func saveRecipeImageToGallery(_ sender: UIBarButtonItem) {
        guard let savingImage = recipeImage.image
        else
        {
            self.view.makeToast("Error: image not found", duration: 2.0)
            return
        }
        
        UIImageWriteToSavedPhotosAlbum(savingImage, nil, nil, nil)
        self.view.makeToast("Saved 🎉", duration: 1.5)
    }
    
}
